package com.example.springboot.services.layers.implementation;


import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.List;

import com.example.springboot.services.UserService;
import com.example.springboot.repositories.UserRepository;
import com.example.springboot.entities.Users;


@Service
public class UserServiceImpl implements UserService
{
	// need a repository object to trigger the base transactions
	@Autowired
	private UserRepository userRepo;


	@Override
	public List<Users> getAllUsers()
	{
		// conversion to List of type Users
		return (List<Users>) userRepo.findAll();
	}




	@Override
	public Users getUserById(int userId)
	{
		return userRepo.findById(userId).orElse(null);
	}





	@Override
	public Users registerUser(Users userdata)
	{
		return userRepo.save(userdata);
	}




	@Override
	public Users updateUser(int userId, Users userdata)
	{
		// TODO update user by Id. Not Working!
		return userRepo.save(userdata);
	}




	@Override
	public String deleteUser(int userId) throws Exception
	{
		Users deleted_user = null;
		try
		{
			deleted_user = userRepo.findById(userId).orElse(null);

			if(deleted_user == null)
			{
				throw new Exception("User Not Available !");
			}
			else
			{
				userRepo.deleteById(userId);
			}
		}
		catch (Exception e)
		{
			throw e;
		}

		return "User " + userId + " has been REMOVED from our access records.";
	}
}
