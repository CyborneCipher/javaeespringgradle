package com.example.springboot.services;

import java.util.List;

import com.example.springboot.entities.Users;

public interface UserService {
	public List<Users> getAllUsers();

	public Users getUserById(int userId);

	public Users registerUser(Users userdata);

	public Users updateUser(int userId, Users userdata);

	public String deleteUser(int userId) throws Exception;
}