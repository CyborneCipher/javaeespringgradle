package com.example.springboot.controllers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import com.example.springboot.entities.Users;
import com.example.springboot.services.UserService;


@RestController
@RequestMapping("/users")
public class UserController 
{
	// Dependency Injection
	// Autowired means spring will create the object of the class
	@Autowired
	private UserService $userService;

	@GetMapping("/all")
	public ResponseEntity<List<Users>> getAllUsers()
	{
		List<Users> users = null;

		try {
			users = $userService.getAllUsers();
		} catch (Exception e) {
			e.getMessage();
		}

		// create a new Response Entity object to send to the frontend
		return new ResponseEntity<List<Users>>(users, HttpStatus.OK);
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<Users> getUserById(@PathVariable("id") int userId)
	{
		Users user = null;

		try {
			user = $userService.getUserById(userId);
		} catch (Exception e) {
			e.getMessage();
		}

		return new ResponseEntity<Users>(user, HttpStatus.OK);
	}
	
	@PostMapping("/user/register/new-user")
	public ResponseEntity<Users> registerNewUser(@RequestBody Users userdata)
	{
		Users user = null;
		try {
			user = $userService.registerUser(userdata);
		} catch (Exception e) {
			e.getMessage();
		}

		return new ResponseEntity<Users>(user, HttpStatus.OK);
	}
	
	@PutMapping("/user/update/{id}")
	public ResponseEntity<String> updateUser(@PathVariable("id") int userId, @RequestBody Users userdata)
	{
		String user = null;

		try {
			// user = $userService.updateUser(userId, userdata);
			user = "Yet to make functionality. TODO: update user by Id. Not Working!";
		} catch (Exception e) {
			e.getMessage();
		}

		return new ResponseEntity<String>(user, HttpStatus.OK);
	}
	

	@DeleteMapping("/users/remove/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable("id") int userId)
	{
		String return_text = "";

		try
		{
			return_text = $userService.deleteUser(userId);
		}
		catch(Exception e)
		{
			e.getMessage();
		}

		return new ResponseEntity<String>(return_text, HttpStatus.OK);
	}
}