package com.example.springboot.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;


@Entity // indicating it is a JPA entity
@Table(name="users")
public class Users
{
	@Id // to make JPA recognise this property as object's id
	@GeneratedValue(strategy= GenerationType.IDENTITY) // to make JPA recognise to auto generate this value
	@Column(name="userId")
	private int userId;

	@Column(name="name")
	private String name; 

	@Column(name="age")
	private int age;

	@Column(name="salary")
	private float salary;

	@Column(name="email")
	private String email;

	// not used directly, so protected
	protected Users() {}

	// used to make instances of user
	public Users(int id, String name, int age, float salary, String email)
	{
		this.userId = id;
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.email = email;
	}

	public int getUserId()
	{
		return this.userId;
	}
	public void setUserId(int id)
	{
		this.userId = id;
	}


	public String getName()
	{
		return this.name;
	}
	public void setName(String name)
	{
		this.name = name;
	}


	public int getAge()
	{
		return this.age;
	}
	public void setAge(int age)
	{
		this.age = age;
	}


	public float getSalary()
	{
		return this.salary;
	}
	public void setSalary(float salary)
	{
		this.salary = salary;
	}


	public String getEmail()
	{
		return this.email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}


	public String toString()
	{
		return String.format("Users [\n\tuserId=%d, \n\tname=%s, \n\tage=%d, \n\tsalary=%f\n\temail: %s]", this.userId, this.name, this.age, this.salary, this.email);
	}
}