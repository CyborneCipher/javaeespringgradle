package com.example.springboot.repositories;


import org.springframework.stereotype.Repository;

import org.springframework.data.repository.CrudRepository;
import com.example.springboot.entities.Users;



@Repository
public interface UserRepository extends CrudRepository<Users, Integer>
{

}